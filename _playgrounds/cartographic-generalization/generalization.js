//==============================================================================
//  General
//==============================================================================

var methods = ["selection", "simplification", "smoothing", "aggregation", "collapse", "exaggeration", "displacement", "classification"];

methods.forEach(function (method) {
  window["svg_" + method] = document.getElementById("gen-" + method);
});
var toggles = document.getElementById("generalization-toggles");

//==============================================================================
//  Setup
//==============================================================================

methods.forEach(function (method) {
  var mdiv = document.createElement("div");
  mdiv.className = "col-6 col-md-3 custom-control custom-checkbox mb-1";
  var minput = document.createElement("input");
  minput.type = "checkbox";
  minput.className = "custom-control-input";
  minput.id = "toggle-" + method;
  var mlabel = document.createElement("label");
  mlabel.className = "custom-control-label";
  mlabel.htmlFor = "toggle-" + method;
  mlabel.innerHTML = capitalize(method);
  mdiv.appendChild(minput);
  mdiv.appendChild(mlabel);
  toggles.appendChild(mdiv);

  minput.addEventListener("change", function () {
    window["svg_" + method].classList.toggle("generalized");
  }, false);
});

//==============================================================================
//  Helper
//==============================================================================

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
