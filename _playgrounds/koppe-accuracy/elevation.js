class Elevation{

  constructor(lng, lat, points){
    this.lng = lng;
    this.lat = lat;
    this.points = points;
  }

  // Open Topo Data
  getOpenTopoData(points){

    // let headers = new Headers();
    // headers.append('Access-Control-Allow-Methods','Get')
    // headers.append('Access-Control-Allow-Origin', 'http://localhost:4000');
    // headers.append('Access-Control-Allow-Credentials', 'true');
    // headers.append('Origin', 'http://localhost:4000');

    var dataset = 'mapzen';
    var opentopo_data = 'https://cartography-playground.opentopodata.org/v1/'+ dataset +'?locations='+ points[0][1] + ',' + points[0][0] + '|' + points[1][1] + ',' + points[1][0] + '|' + points[2][1] + ',' + points[2][0] +  '&interpolation=cubic';
    return new Promise((resolve) => {
      fetch(opentopo_data, {
        method: 'GET',
        // mode: 'cors',
        // credentials: 'include',
        contentType: 'application/json',
        // headers: headers,
      })
      .then(response => response.json())
      .then(data => resolve(data))
      // .catch(err => reject(err))
      .catch(err =>{ alert(`⚠️
          Cannot get openTopoData: Error of
          ${err.error}`)
        });
    })
    
/*
    return $.ajax({
      method: 'GET',
      url: opentopo_data,
      // mode: 'cors',
      // credentials: 'include',
      // headers: headers,
      // origin: 'http://localhost:4000',
      // contentType: 'application/x-www-form-urlencoded',
      // accept: 'application/json',

    })
    .done(function(data){
      var results = data.elevation;
      return results;
    })
    .fail(function(err){
      console.log(err.error);
      alert("⚠️\n Cannot get openTopoData: Error of\n" + err.error);
    });
  */

  };

/*
// == Additional Elevation Data ================= currently broken :(
*/

  // Open Elevation
  getOpenElevation(lng, lat) {
  
    var openelevation_data = {"locations":[]};
    openelevation_data.locations.push({"latitude": lat, "longitude": lng})     
    $.ajax({
      url: "https://api.open-elevation.com/api/v1/lookup",
      method: "POST",
      contentType: "application/json",
      dataType: "json",
      data: JSON.stringify(openelevation_data)
    })
    .done(function(json) {
      const elev = [];
      json.results.map(function(res) {
        elev.push(res.elevation);
      });
      console.log(elev);
    })
    .fail(function(err) {
      console.log(err);
      alert("⚠️\nCannot show elevation: request failed because\n" + err.statusText);
    });
  
  };
  


    // Elevation with Mapbox
    getMapboxElevation() {
      // Construct the API request
      var query = 'https://api.mapbox.com/v4/mapbox.mapbox-terrain-v2/tilequery/' + lng + ',' + lat + '.json?layers=contour&limit=50&access_token=' + mapboxgl.accessToken;
      return $.ajax({
        method: 'GET',
        url: query,
      })
    };
  
    highestElev(data){  // Get all the returned features
      const allFeatures = data.features;
      // console.log(allFeatures);
      // Create an empty array to add elevation data to
      var elevations = [];
  
      // For each returned feature, add elevation data to the elevations array
      for (i = 0; i < allFeatures.length; i++) {
        elevations.push(allFeatures[i].properties.ele);
      }
      // console.log(elevations);
  
      // In the elevations array, find the largest value
      var data = Math.max(...elevations);
      console.log(data);
      // var lowestElevation = Math.min(...elevations);
      return data; 
    };

}